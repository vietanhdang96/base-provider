import 'package:base_provider/base/base_viewmodel.dart';
import 'package:base_provider/main.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class BaseStatelessPage<VM extends BaseViewModel>
    extends StatelessWidget {
  @override
  Widget build(BuildContext pageContext) {
    return ChangeNotifierProvider(
      create: (_) => getIt<VM>(),
      builder: (BuildContext context, _) {
        final VM viewModel = Provider.of<VM>(
          context,
          listen: false,
        );
        return buildView(
          pageContext,
          viewModel,
        );
      },
    );
  }

  Widget buildView(BuildContext pageContext, VM viewModel);
}
