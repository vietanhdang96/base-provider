import 'package:flutter/foundation.dart';

abstract class BaseViewModel extends ChangeNotifier {
  @override
  void dispose() {
    super.dispose();
    print('base model disposed');
  }
}
