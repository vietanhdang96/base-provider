import 'package:base_provider/base/base_viewmodel.dart';
import 'package:base_provider/main.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class BasePageState<VM extends BaseViewModel, P extends StatefulWidget>
    extends State<P> {
  @protected
  VM viewModel = getIt<VM>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: viewModel,
      child: buildView(context),
    );
  }

  @override
  void dispose() {
    super.dispose();
    viewModel.dispose();
  }

  Widget buildView(BuildContext pageContext);
}
