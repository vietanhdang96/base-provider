import 'package:base_provider/page/detail/detail_page.dart';
import 'package:base_provider/page/gif/gif_page.dart';
import 'package:base_provider/page/home/home_page.dart';
import 'package:base_provider/page/multilvel_list/multilevel_list_page.dart';
import 'package:sailor/sailor.dart';

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes([
      SailorRoute(
        name: "/homePage",
        builder: (context, args, params) {
          return const HomePage();
        },
      ),
      SailorRoute(
        name: "/detailPage",
        builder: (context, args, params) {
          return DetailPage();
        },
      ),
      SailorRoute(
        name: "/multiLevelListPage",
        builder: (context, args, params) {
          return MultiLevelListPage();
        },
      ),
      SailorRoute(
        name: "/gifPage",
        builder: (context, args, params) {
          return GifPage();
        },
      ),
    ]);
  }
}
