import 'package:base_provider/util/color.dart';
import 'package:flutter/material.dart';

class LinearGradientBox extends StatelessWidget {
  final Widget child;
  final BorderRadius borderRadius;
  final List<Color> colors;

  const LinearGradientBox({
    Key key,
    @required this.child,
    this.borderRadius = const BorderRadius.all(
      Radius.circular(11.0),
    ),
    this.colors = const [
      ColorUtils.yellow,
      ColorUtils.pink,
    ],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: borderRadius,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          gradient: LinearGradient(
            colors: colors,
          ),
        ),
        child: child,
      ),
    );
  }
}
