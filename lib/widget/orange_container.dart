import 'package:flutter/material.dart';

class OrangeContainer extends StatefulWidget {
  @override
  _OrangeContainerState createState() => _OrangeContainerState();
}

class _OrangeContainerState extends State<OrangeContainer> {

  @override
  void initState() {
    super.initState();
    print('init OrangeContainer');
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose OrangeContainer');

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.deepOrange,
    );
  }
}
