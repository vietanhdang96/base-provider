import 'package:flutter/material.dart';

class RedContainer extends StatefulWidget {
  @override
  _RedContainerState createState() => _RedContainerState();
}

class _RedContainerState extends State<RedContainer> {
  @override
  void initState() {
    super.initState();
    print('init RedContainer');
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose RedContainer');

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
    );
  }
}
