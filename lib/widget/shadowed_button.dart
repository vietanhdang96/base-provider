import 'package:base_provider/util/color.dart';
import 'package:flutter/material.dart';

class ShadowedButton extends StatelessWidget {
  final Widget child;
  final GestureTapCallback onTap;
  final Color color;
  final Offset offset;
  final BorderRadius borderRadius;
  final double blurRadius;

  const ShadowedButton({
    @required this.child,
    @required this.onTap,
    this.color = ColorUtils.pink,
    this.offset = const Offset(0.0, 10.0),
    this.borderRadius = const BorderRadius.all(
      Radius.circular(90.0),
    ),
    this.blurRadius = 30.0,
  });

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        color: color,
        borderRadius: borderRadius,
        boxShadow: [
          BoxShadow(
            color: color,
            blurRadius: blurRadius,
            offset: offset,
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: Stack(
          children: <Widget>[
            child,
            Positioned.fill(
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: onTap,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
