import 'package:flutter/material.dart';

class ColorView extends StatelessWidget {
  final double height;
  final double width;
  final Color color;

  const ColorView(this.height, this.width, {Key key, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: color != null
          ? DecoratedBox(
              decoration: BoxDecoration(
                color: color,
              ),
            )
          : null,
    );
  }
}
