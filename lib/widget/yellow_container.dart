import 'package:flutter/material.dart';

class YellowContainer extends StatefulWidget {
  @override
  _YellowContainerState createState() => _YellowContainerState();
}

class _YellowContainerState extends State<YellowContainer> {
  @override
  void initState() {
    super.initState();
    print('init YellowContainer');
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose YellowContainer');

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellow,
    );
  }
}
