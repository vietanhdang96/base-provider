import 'package:flutter/material.dart';

class BlueContainer extends StatefulWidget {
  @override
  _BlueContainerState createState() => _BlueContainerState();
}

class _BlueContainerState extends State<BlueContainer> {
  @override
  void initState() {
    super.initState();
    print('init BlueContainer');
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose BlueContainer');
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
    );
  }
}
