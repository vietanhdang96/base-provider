import 'package:base_provider/util/color.dart';
import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  const CustomCheckbox({
    Key key,
    this.isChecked,
    this.isBackgroundDisplaying,
  }) : super(key: key);

  final bool isChecked;
  final bool isBackgroundDisplaying;

  @override
  State<StatefulWidget> createState() => CustomCheckboxState();
}

class CustomCheckboxState extends State<CustomCheckbox> {
  bool isChecked = false;
  bool isBackgroundDisplaying = false;

  @override
  void initState() {
    super.initState();
    isChecked = widget.isChecked;
    isBackgroundDisplaying = widget.isBackgroundDisplaying;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 24.0,
      height: 24.0,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          border: Border.all(
            color: ColorUtils.pink,
            width: 2.0,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: () {
                if (isChecked) {
                  return ColorUtils.pink;
                } else {
                  if (isBackgroundDisplaying) {
                    return ColorUtils.pink.withOpacity(0.4);
                  } else {
                    return Colors.transparent;
                  }
                }
              }(),
            ),
            child: Visibility(
              visible: isChecked,
              child: const Icon(
                Icons.check,
                size: 22.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool toggleCheckState() {
    setState(() {
      isChecked = !isChecked;
    });
    return isChecked;
  }

  void toggleBackgroundState({bool isDisplaying}) {
    setState(() {
      isBackgroundDisplaying = isDisplaying;
    });
  }
}
