import 'package:base_provider/main.dart';
import 'package:base_provider/page/multilvel_list/widget/custom_checkbox.dart';
import 'package:base_provider/util/color.dart';
import 'package:base_provider/widget/color_view.dart';
import 'package:base_provider/widget/custom_expansion_tile.dart';
import 'package:flutter/material.dart';

class MultiLevelListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'multi level list',
        ),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.only(right: 24.0),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) => EntryItem(
          data[index],
        ),
      ),
    );
  }
}

class EntryItem extends StatefulWidget {
  const EntryItem(this.entry);

  final Entry entry;

  @override
  _EntryItemState createState() => _EntryItemState();
}

class _EntryItemState extends State<EntryItem>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _buildTiles(
      widget.entry,
      updateCheckedChildrenContainingState: () {},
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildTiles(
    Entry item, {
    bool isDividerPadded = false,
    Function updateCheckedChildrenContainingState,
  }) {
    final GlobalKey<CustomCheckboxState> itemCheckboxKey =
        GlobalKey<CustomCheckboxState>();

    if (item.childrens.isEmpty) {
      return Column(
        children: [
          InkWell(
            onTap: () {
              item.isChecked = itemCheckboxKey.currentState.toggleCheckState();
              if (updateCheckedChildrenContainingState != null) {
                updateCheckedChildrenContainingState();
              }
            },
            child: SizedBox(
              height: 57.0,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, right: 12.0),
                    child: CustomCheckbox(
                      key: itemCheckboxKey,
                      isChecked: item.isChecked,
                      isBackgroundDisplaying: item.isContainingCheckedChildren,
                    ),
                  ),
                  Text(item.title),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: isDividerPadded ? 60.0 : 24.0),
            child: const ColorView(
              1.0,
              double.infinity,
              color: Colors.black,
            ),
          ),
        ],
      );
    }
    return CustomExpansionTile(
      // key: PageStorageKey<Entry>(root),
      content: Column(
        children: [
          InkWell(
            onTap: () {
              item.isChecked = itemCheckboxKey.currentState.toggleCheckState();
              if (updateCheckedChildrenContainingState != null) {
                updateCheckedChildrenContainingState();
              }
            },
            child: SizedBox(
              height: 57.0,
              child: Row(
                children: [
                  // ExpandCollapseIcon(key: expandIconKey),
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, right: 12.0),
                    child: CustomCheckbox(
                      key: itemCheckboxKey,
                      isChecked: item.isChecked,
                      isBackgroundDisplaying: item.isContainingCheckedChildren,
                    ),
                  ),
                  Text(item.title),
                ],
              ),
            ),
          ),
        ],
      ),
      divider: Padding(
        padding: EdgeInsets.only(left: isDividerPadded ? 60.0 : 24.0),
        child: const ColorView(
          1.0,
          double.infinity,
          color: Colors.black,
        ),
      ),
      childrenPadding: const EdgeInsets.only(left: 24.0),
      collapseIcon: const SizedBox(
        height: 57.0,
        width: 57.0,
        child: Icon(Icons.add, color: ColorUtils.pink),
      ),
      expandIcon: const SizedBox(
        height: 57.0,
        width: 57.0,
        child: Icon(Icons.remove, color: ColorUtils.pink),
      ),
      children: item.childrens.map<Widget>((Entry children) {
        return _buildTiles(
          children,
          isDividerPadded: true,
          updateCheckedChildrenContainingState:
              getUpdateCheckedChildrenContainingState(
            item,
            itemCheckboxKey,
            updateCheckedChildrenContainingState,
          ),
        );
      }).toList(),
    );
  }

  Function getUpdateCheckedChildrenContainingState(
    Entry item,
    GlobalKey<CustomCheckboxState> customCheckboxKey,
    Function updateCheckedChildrenContainingState,
  ) {
    return () {
      if (item.childrens.isNotEmpty) {
        final int checkedItem = item.childrens.indexWhere(
          (Entry children) {
            return children.isChecked || children.isContainingCheckedChildren;
          },
        );
        item.isContainingCheckedChildren = checkedItem != -1;
        final CustomCheckboxState checkboxState =
            customCheckboxKey.currentState;
        if (checkboxState != null) {
          checkboxState.toggleBackgroundState(
            isDisplaying: item.isContainingCheckedChildren,
          );
        }
      }

      if (updateCheckedChildrenContainingState != null) {
        updateCheckedChildrenContainingState();
      }
    };
  }
}
