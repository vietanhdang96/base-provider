import 'package:base_provider/base/base_stateless_page.dart';
import 'package:base_provider/page/detail/detail_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailPage extends BaseStatelessPage<DetailViewModel> {
  @override
  Widget buildView(BuildContext pageContext, DetailViewModel viewModel) {
    return Scaffold(
      body: SizedBox.expand(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Selector<DetailViewModel, int>(
                selector: (_, DetailViewModel viewModel) {
                  return viewModel.firstDetailValue;
                },
                builder: (_, int data, Widget child) {
                  return Column(
                    children: [
                      Text('first detail value $data'),
                      child,
                    ],
                  );
                },
                child: Selector<DetailViewModel, int>(
                  selector: (_, DetailViewModel viewModel) {
                    return viewModel.secondDetailValue;
                  },
                  builder: (_, int data, Widget child) {
                    return Column(
                      children: [
                        Text('second detail value $data'),
                        child,
                      ],
                    );
                  },
                  child: Text('constant detail text'),
                ),
              ),
              Consumer<DetailViewModel>(
                builder: (_, DetailViewModel viewModel, __) {
                  return Text(
                    'consume second detail value ${viewModel.secondDetailValue}',
                  );
                },
              ),
              FlatButton(
                onPressed: () {
                  viewModel.increment();
                },
                child: Text('increment detail value'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
