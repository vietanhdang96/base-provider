import 'package:base_provider/base/base_viewmodel.dart';

class DetailViewModel extends BaseViewModel {
  int _firstDetailValue = 0;
  int _secondDetailValue = 1;

  int get firstDetailValue => _firstDetailValue;

  int get secondDetailValue => _secondDetailValue;

  void increment() {
    _firstDetailValue++;
    _secondDetailValue += 2;
    notifyListeners();
  }
}
