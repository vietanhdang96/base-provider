import 'dart:typed_data';
import 'dart:ui';

import 'package:base_provider/util/size.dart';
import 'package:base_provider/widget/blue_container.dart';
import 'package:base_provider/widget/orange_container.dart';
import 'package:base_provider/widget/red_container.dart';
import 'package:base_provider/widget/yellow_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';

class GifPage extends StatefulWidget {
  @override
  _GifPageState createState() => _GifPageState();
}

class _GifPageState extends State<GifPage> with TickerProviderStateMixin {
  String gifAssetLink = '';
  GifController gifStickerController;
  final double gifStickerOpacity = 0.8;
  bool displayContainer = false;
  int stackIndex = 0;

  @override
  void initState() {
    super.initState();
    gifStickerController = GifController(vsync: this)
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          print('completed');
          setState(() {
            gifAssetLink = '';
          });
        }
      });
    // gif();
  }

  gif() async {
    final ByteData bytes = await rootBundle.load('assets/gif/second.gif');
    final Uint8List list = bytes.buffer.asUint8List();
    var codec = await instantiateImageCodec(list);
    var frame = codec.frameCount;
    print(frame);
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = SizeUtils.getScreenWidth(context);
    double screenHeight = SizeUtils.getScreenHeight(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'gif page',
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              alignment: Alignment.center,
              children: [
                const SizedBox.expand(
                  child: Image(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/image/water.jpg'),
                  ),
                ),
                Visibility(
                  visible: gifAssetLink.isNotEmpty,
                  child: Opacity(
                    opacity: gifStickerOpacity,
                    child: GifImage(
                      image: AssetImage(gifAssetLink),
                      controller: gifStickerController,
                    ),
                  ),
                ),
                AnimatedPositioned(
                  duration: Duration(milliseconds: 200),
                  right: displayContainer ? 0 : -screenWidth,
                  bottom: 0,
                  child: SizedBox(
                    width: screenWidth,
                    height: screenHeight,
                    child: ListView(
                      children: [
                        Container(
                          height: SizeUtils.getScreenHeight(context) / 2,
                          width: SizeUtils.getScreenWidth(context),
                          color: Colors.black,
                        ),
                        Container(
                          height: SizeUtils.getScreenHeight(context) / 2,
                          width: SizeUtils.getScreenWidth(context),
                          color: Colors.orange,
                        ),
                        Container(
                          height: SizeUtils.getScreenHeight(context) / 2,
                          width: SizeUtils.getScreenWidth(context),
                          color: Colors.blueAccent,
                        ),
                      ],
                    ),
                  ),
                ),
                IndexedStack(
                  index: stackIndex,
                  children: [
                    OrangeContainer(),
                    BlueContainer(),
                    RedContainer(),
                    YellowContainer(),
                  ],
                )
              ],
            ),
          ),
          Column(
            children: [
              FlatButton(
                onPressed: () async {
                  final ByteData bytes =
                      await rootBundle.load('assets/gif/first.gif');
                  final Uint8List list = bytes.buffer.asUint8List();
                  var codec = await instantiateImageCodec(list);
                  final int frame = codec.frameCount;
                  double targetFrame = frame.toDouble();
                  setState(() {
                    gifStickerController.reset();
                    gifAssetLink = 'assets/gif/first.gif';
                    gifStickerController.animateTo(
                      targetFrame,
                      duration: Duration(milliseconds: frame * 100),
                    );
                  });
                },
                child: const Text('display first gif'),
              ),
              FlatButton(
                onPressed: () {
                  setState(() {
                    // displayContainer = !displayContainer;
                    if (stackIndex == 3) {
                      stackIndex = 0;
                    } else {
                      stackIndex++;
                    }
                  });
                },
                child: const Text('display ct'),
              ),
              FlatButton(
                onPressed: () async {
                  final ByteData bytes =
                      await rootBundle.load('assets/gif/second.gif');
                  final Uint8List list = bytes.buffer.asUint8List();
                  var codec = await instantiateImageCodec(list);
                  final int frame = codec.frameCount;
                  double targetFrame = frame.toDouble();
                  setState(() {
                    gifStickerController.reset();
                    gifAssetLink = 'assets/gif/second.gif';
                    gifStickerController.animateTo(
                      targetFrame,
                      duration: Duration(milliseconds: frame * 100),
                    );
                  });
                },
                child: Text('display second gif'),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    gifStickerController.dispose();
    super.dispose();
  }
}
