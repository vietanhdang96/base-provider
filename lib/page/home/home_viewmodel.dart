import 'package:base_provider/base/base_viewmodel.dart';

class HomeViewModel extends BaseViewModel {
  int _firstValue = 0;

  int get firstValue => _firstValue;

  int _secondValue = 1;

  int get secondValue => _secondValue;

  void increment() {
    _firstValue++;
    _secondValue += 2;
    notifyListeners();
  }
}
