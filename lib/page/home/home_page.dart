import 'dart:io';

import 'package:base_provider/base/base_page_state.dart';
import 'package:base_provider/page/home/home_viewmodel.dart';
import 'package:base_provider/routes.dart';
import 'package:base_provider/util/permission.dart';
import 'package:base_provider/widget/linear_gradient_box.dart';
import 'package:base_provider/widget/shadowed_button.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends BasePageState<HomeViewModel, HomePage> {
  @override
  Widget buildView(BuildContext pageContext) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Selector<HomeViewModel, int>(
              selector: (_, HomeViewModel viewModel) {
                return viewModel.firstValue;
              },
              builder: (_, int data, Widget child) {
                return Column(
                  children: [
                    Text('first value $data'),
                    child,
                  ],
                );
              },
              child: LinearGradientBox(
                child: Selector<HomeViewModel, int>(
                  selector: (_, HomeViewModel viewModel) {
                    return viewModel.secondValue;
                  },
                  builder: (_, int data, Widget child) {
                    return Column(
                      children: [
                        Text('second value $data'),
                        child,
                      ],
                    );
                  },
                  child: Text('constant text'),
                ),
              ),
            ),
            Consumer<HomeViewModel>(
              builder: (_, HomeViewModel viewModel, __) {
                return Text('consume second value ${viewModel.secondValue}');
              },
            ),
            FlatButton(
              onPressed: () {
                viewModel.increment();
              },
              child: Text('increment'),
            ),
            FlatButton(
              onPressed: () {
                Routes.sailor.navigate("/detailPage");
              },
              child: Text('navigate to detail page'),
            ),
            FlatButton(
              onPressed: () {
                Routes.sailor.navigate("/multiLevelListPage");
              },
              child: Text('navigate to multi level list page'),
            ),
            ShadowedButton(
              onTap: () {
                Routes.sailor.navigate("/gifPage");
              },
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text('navigate to gif page'),
              ),
            ),
            Builder(
              builder: (BuildContext context) {
                return FlatButton(
                  onPressed: () {
                    PermissionUtil.requestPermission(
                      permission: Platform.isAndroid
                          ? Permission.storage
                          : Permission.photos,
                      onGranted: () {
                        print('granted');
                      },
                      onPermanentlyDenied: () {
                        print('onPermanentlyDenied');
                        final snackBar = SnackBar(
                          content: const Text(
                            'Bạn cần cấp quyền truy cập photo',
                          ),
                          action: SnackBarAction(
                            label: 'Mở cài đặt ứng dụng',
                            onPressed: () async {
                              await openAppSettings();
                            },
                          ),
                        );
                        Scaffold.of(context)
                          ..removeCurrentSnackBar()
                          ..showSnackBar(snackBar);
                      },
                      onRestricted: () {
                        print('onRestricted');
                      },
                    );
                  },
                  child: Text('request photo permission'),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
