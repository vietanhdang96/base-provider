import 'package:base_provider/page/detail/detail_viewmodel.dart';
import 'package:base_provider/page/home/home_page.dart';
import 'package:base_provider/page/home/home_viewmodel.dart';
import 'package:base_provider/routes.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

class Entry {
  final String title;
  bool isChecked;
  bool isContainingCheckedChildren;
  final List<Entry>
  childrens; // Since this is an expansion list ...children can be another list of entries
  Entry(this.title, this.isChecked, this.isContainingCheckedChildren,
      [this.childrens = const <Entry>[]]);
}

// This is the entire multi-level list displayed by this app
final List<Entry> data = <Entry>[
  Entry(
    'Chapter A',
    false,
    false,
    <Entry>[
      Entry(
        'Section A0',
        false,
        false,
        <Entry>[
          Entry(
            'Item A0.1',
            false,
            false,
            <Entry>[
              Entry(
                'Item A0.1.1',
                false,
                false,
                <Entry>[
                  Entry(
                    'Item A0.1.1.1',
                    false,
                    false,
                  ),
                  Entry(
                    'Item A0.1.1.2',
                    false,
                    false,
                  ),
                  Entry(
                    'Item A0.1.1.3',
                    false,
                    false,
                  ),
                ],
              ),
              Entry(
                'Item A0.1.2',
                false,
                false,
                <Entry>[
                  Entry(
                    'Item A0.1.2.1',
                    false,
                    false,
                  ),
                  Entry(
                    'Item A0.1.2.2',
                    false,
                    false,
                  ),
                  Entry(
                    'Item A0.1.2.3',
                    false,
                    false,
                  ),
                ],
              ),
              Entry(
                'Item A0.1.3',
                false,
                false,
              ),
            ],
          ),
          Entry(
            'Item A0.2',
            false,
            false,
            <Entry>[
              Entry(
                'Item A0.2.1',
                false,
                false,
              ),
              Entry(
                'Item A0.2.2',
                false,
                false,
              ),
              Entry(
                'Item A0.2.3',
                false,
                false,
              ),
            ],
          ),
          Entry(
            'Item A0.3',
            false,
            false,
          ),
        ],
      ),
      Entry(
        'Section A1',
        false,
        false,
      ),
      Entry(
        'Section A2',
        false,
        false,
      ),
    ],
  ),
  // Second Row
  Entry('Chapter B', false, false, <Entry>[
    Entry(
      'Section B0',
      false,
      false,
    ),
    Entry(
      'Section B1',
      false,
      false,
    ),
  ]),
  Entry(
    'Chapter C',
    false,
    false,
    <Entry>[
      Entry(
        'Section C0',
        false,
        false,
      ),
      Entry(
        'Section C1',
        false,
        false,
      ),
      Entry(
        'Section C2',
        false,
        false,
        <Entry>[
          Entry(
            'Item C2.0',
            false,
            false,
          ),
          Entry(
            'Item C2.1',
            false,
            false,
          ),
          Entry(
            'Item C2.2',
            false,
            false,
          ),
          Entry(
            'Item C2.3',
            false,
            false,
          ),
        ],
      )
    ],
  ),
];

void setupServiceLocator() {
  registerViewModels();
  registerOtherThings();
}

void registerViewModels() {
  getIt
    ..registerFactory(
      () => HomeViewModel(),
    )
    ..registerFactory(
      () => DetailViewModel(),
    );
}

void registerOtherThings() {}

void main() async {
  setupServiceLocator();
  Routes.createRoutes();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
      navigatorKey: Routes.sailor.navigatorKey,
      onGenerateRoute: Routes.sailor.generator(),
    );
  }
}
