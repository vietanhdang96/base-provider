import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionUtil {
  static void _onDeniedButtonPressed() {
    print('_onDeniedButtonPressed');
  }

  static void _onPermanentlyDeniedButtonPressed() {
    print('_onPermanentlyDeniedButtonPressed');
  }

  static void requestPermission({
    @required Permission permission,
    @required Function onGranted,
    @required Function onPermanentlyDenied,
    @required Function onRestricted,
    Function onDeniedButtonPressed = _onDeniedButtonPressed,
    Function onPermanentlyDeniedButtonPressed =
        _onPermanentlyDeniedButtonPressed,
  }) {
    switch (Platform.operatingSystem) {
      case "android":
        requestPermissionOnAndroid(
          permission: permission,
          onGranted: onGranted,
          onPermanentlyDenied: onPermanentlyDenied,
          onPermanentlyDeniedButtonPressed: onPermanentlyDeniedButtonPressed,
          onDeniedButtonPressed: onDeniedButtonPressed,
        );
        break;
      case "ios":
        requestPermissionOnIOS(
          permission: permission,
          onGranted: onGranted,
          onPermanentlyDenied: onPermanentlyDenied,
          onPermanentlyDeniedButtonPressed: onPermanentlyDeniedButtonPressed,
          onRestricted: onRestricted,
        );
        break;
      default:
    }
  }

  static Future<void> requestPermissionOnIOS({
    @required Permission permission,
    @required Function onGranted,
    @required Function onRestricted,
    @required Function onPermanentlyDenied,
    Function onPermanentlyDeniedButtonPressed =
        _onPermanentlyDeniedButtonPressed,
  }) async {
    try {
      final PermissionStatus permissionStatus = await permission.status;

      switch (permissionStatus) {
        case PermissionStatus.granted:
          onGranted();
          break;
        case PermissionStatus.permanentlyDenied:
          onPermanentlyDenied();
          break;
        case PermissionStatus.denied:
          onPermanentlyDenied();
          break;
        case PermissionStatus.restricted:
          onRestricted();
          break;
        case PermissionStatus.undetermined:
          final PermissionStatus permissionRequestResult =
              await permission.request();

          if (permissionRequestResult.isGranted) {
            onGranted();
          } else {
            onPermanentlyDeniedButtonPressed();
          }
          break;
        default:
      }
    } catch (exception) {
      onPermanentlyDenied();
    }
  }

  static Future<void> requestPermissionOnAndroid({
    @required Permission permission,
    @required Function onGranted,
    @required Function onPermanentlyDenied,
    Function onDeniedButtonPressed = _onDeniedButtonPressed,
    Function onPermanentlyDeniedButtonPressed =
        _onPermanentlyDeniedButtonPressed,
  }) async {
    try {
      final PermissionStatus permissionStatus = await permission.status;

      switch (permissionStatus) {
        case PermissionStatus.granted:
          onGranted();
          break;
        case PermissionStatus.permanentlyDenied:
          onPermanentlyDenied();
          break;
        case PermissionStatus.denied:
          if (await permission.shouldShowRequestRationale) {
            final PermissionStatus permissionRequestResult =
                await permission.request();

            if (permissionRequestResult.isGranted) {
              onGranted();
            } else if (permissionRequestResult.isDenied) {
              onDeniedButtonPressed();
            } else {
              onPermanentlyDeniedButtonPressed();
            }
          } else {
            onPermanentlyDenied();
          }
          break;
        case PermissionStatus.restricted:
          break;
        case PermissionStatus.undetermined:
          final PermissionStatus permissionRequestResult =
              await permission.request();

          if (permissionRequestResult.isGranted) {
            onGranted();
          } else {
            onDeniedButtonPressed();
          }
          break;
        default:
      }
    } catch (exception) {
      onPermanentlyDenied();
    }
  }
}
